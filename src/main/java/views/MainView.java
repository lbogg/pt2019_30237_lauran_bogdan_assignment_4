package views;

import logical.misc.RestaurantProcessing;
import logical.users.interfaces.AdminGUI;
import logical.users.interfaces.ChefGUI;
import logical.users.interfaces.WaiterGUI;

import javax.swing.*;
import java.awt.*;

public class MainView extends JFrame {

    private JPanel jPanel;
    private JButton adminBtn, waiterBtn, chefBtn;

    private RestaurantProcessing restaurantProcessing;
    private AdminGUI admin;
    private WaiterGUI waiter;
   // private ChefGUI chef;

    public MainView(RestaurantProcessing restaurantProcessing, AdminGUI admin, WaiterGUI waiter, ChefGUI chef){
        this.setSize(new Dimension(300,300));
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setTitle("Restaurant");
        this.setLocationRelativeTo(null);

        this.restaurantProcessing = restaurantProcessing;
        this.admin = admin;
        this.waiter = waiter;
        //this.chef = chef;

        initElements();
    }

    private void initElements(){
        this.adminBtn = new JButton("Admin");
        this.waiterBtn = new JButton("Waiter");
        //this.chefBtn = new JButton("Chef");
        this.jPanel = new JPanel();
        createListeners();
    }

    private void createListeners(){
        GroupLayout layout = new GroupLayout(jPanel);
        jPanel.setLayout(layout);
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);
        GroupLayout.SequentialGroup hGroup = layout.createSequentialGroup();
        hGroup.addGroup(layout.createParallelGroup().addComponent(adminBtn).addComponent(waiterBtn));

        layout.setHorizontalGroup(hGroup);

        GroupLayout.SequentialGroup vGroup = layout.createSequentialGroup();
        vGroup.addGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER).addComponent(adminBtn));
        vGroup.addGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER).addComponent(waiterBtn));
        //vGroup.addGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER).addComponent(chefBtn));

        layout.setVerticalGroup(vGroup);
        this.add(jPanel);

        adminBtn.addActionListener(e -> {
            AdministratorOperations administratorOperations = new AdministratorOperations(restaurantProcessing, admin);
            administratorOperations.setVisible(true);
        });
        waiterBtn.addActionListener(e -> {
            WaiterOperations waiterOperations = new WaiterOperations(restaurantProcessing,waiter);
            waiterOperations.setVisible(true);
        });
    }
}
