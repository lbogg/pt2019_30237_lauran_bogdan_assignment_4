package views;

import logical.misc.RestaurantProcessing;
import logical.pojos.BaseProduct;
import logical.pojos.CompositeProduct;
import logical.pojos.interfaces.AbstractMenuItem;
import logical.pojos.interfaces.MenuItem;
import logical.users.interfaces.AdminGUI;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import java.awt.*;
import java.util.*;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;


public class AdministratorOperations extends JFrame {

    private JPanel jPanel;
    private JButton addBtn, updateBtn, viewAllBtn, deleteBtn;
    private JTextField produsTxt, produsTxt1, produsTxt2, nameTxt, priceTxt, priceTxt1, priceTxt2;
    private RestaurantProcessing restaurantProcessing;
    private AdminGUI admin;
    private List<MenuItem> menuItems;

    public AdministratorOperations(RestaurantProcessing restaurantProcessing, AdminGUI admin){
        this.setSize(new Dimension(400,400));
        this.setTitle("Admin operations");
        this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

        this.restaurantProcessing = restaurantProcessing;
        this.admin = admin;

        initElements();
    }

    private void initElements(){
        this.addBtn = new JButton("ADD");
        this.updateBtn = new JButton("UPDATE");
        this.viewAllBtn = new JButton("SHOW ALL");
        this.deleteBtn = new JButton("DELETE");
        this.produsTxt = new JTextField("Produs");
        this.produsTxt1 = new JTextField("Produs");
        this.produsTxt2 = new JTextField("Produs");
        this.nameTxt = new JTextField("MenuItem name");
        this.priceTxt = new JTextField("Price");
        this.priceTxt1 = new JTextField("Price");
        this.priceTxt2 = new JTextField("Price");
        this.jPanel = new JPanel();
        this.menuItems = restaurantProcessing.getRestaurant().getMenu();
        addListeners();
    }

    private void addListeners(){
        GroupLayout layout = new GroupLayout(jPanel);
        jPanel.setLayout(layout);
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);
        GroupLayout.SequentialGroup hGroup = layout.createSequentialGroup();

        hGroup.addGroup(layout.createParallelGroup().addComponent(produsTxt).addComponent(priceTxt).addComponent(produsTxt1).addComponent(priceTxt1).addComponent(produsTxt2).addComponent(priceTxt2).addComponent(nameTxt).addComponent(addBtn).addComponent(updateBtn).addComponent(deleteBtn).addComponent(viewAllBtn));
       /* hGroup.addGroup(layout.createParallelGroup().addComponent(addBtn));
        hGroup.addGroup(layout.createParallelGroup().addComponent(updateBtn));
        hGroup.addGroup(layout.createParallelGroup().addComponent(deleteBtn));
        hGroup.addGroup(layout.createParallelGroup().addComponent(viewAllBtn));*/
        layout.setHorizontalGroup(hGroup);

        GroupLayout.SequentialGroup vGroup = layout.createSequentialGroup();
        vGroup.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                .addComponent(produsTxt))
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                .addComponent(priceTxt))
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                .addComponent(produsTxt1))
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                        .addComponent(priceTxt1))
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                        .addComponent(produsTxt2))
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                        .addComponent(priceTxt2))
                                ));
        /*vGroup.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(priceTxt).addComponent(priceTxt1));
        vGroup.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(produsTxt2).addComponent(priceTxt2));*/
        vGroup.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(nameTxt));
        vGroup.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(layout.createSequentialGroup().addComponent(addBtn).addComponent(updateBtn).addComponent(deleteBtn).addComponent(viewAllBtn)));
        layout.setVerticalGroup(vGroup);

        this.add(jPanel);

        this.addBtn.addActionListener(e -> {
            BaseProduct baseProduct = new BaseProduct(produsTxt.getText(), Double.parseDouble(priceTxt.getText()));
            BaseProduct baseProduct1 = new BaseProduct(produsTxt1.getText(), Double.parseDouble(priceTxt1.getText()));
            BaseProduct baseProduct2 = new BaseProduct(produsTxt2.getText(), Double.parseDouble(priceTxt2.getText()));

            CompositeProduct product = new CompositeProduct(nameTxt.getText(), Arrays.asList(baseProduct, baseProduct1, baseProduct2));

            admin.saveMenuItem(product);
            admin.saveMenuItem(baseProduct);
            admin.saveMenuItem(baseProduct1);
            admin.saveMenuItem(baseProduct2);

            restaurantProcessing.saveData();
        });

        this.updateBtn.addActionListener(e -> {

            for(MenuItem item: menuItems) {
                if(item.getName().equals(nameTxt.getText())) {
                    CompositeProduct compositeProduct = ((CompositeProduct) item);

                    compositeProduct.getComposedOf().get(0)
                            .setPrice(Double.parseDouble(priceTxt.getText()))
                            .setName(produsTxt.getText());

                    compositeProduct.getComposedOf().get(1)
                            .setPrice(Double.parseDouble(priceTxt1.getText()))
                            .setName(produsTxt1.getText());

                    compositeProduct.getComposedOf().get(2)
                            .setPrice(Double.parseDouble(priceTxt2.getText()))
                            .setName(produsTxt2.getText());
                }
            }

//            menuItems.forEach(admin::saveMenuItem);
            restaurantProcessing.saveData();
        });

        this.deleteBtn.addActionListener(e -> {
            for(MenuItem m: menuItems){
                if(m.getName().equals(nameTxt.getText())){
                    admin.deleteMenuItem(m);

                    if (m instanceof CompositeProduct) {
                        ((CompositeProduct) m).getComposedOf().forEach(admin::deleteMenuItem);
                    }
                }
            }
            restaurantProcessing.saveData();
        });

        this.viewAllBtn.addActionListener(e -> {
            JFrame cViewFrame = new JFrame();
            cViewFrame.setSize(new Dimension(800,400));
            cViewFrame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

            String[] columns = {"Id","Nume","Pret"};
            Object[][] obj = new Object[menuItems.size()][3];

            for (int i = 0; i < obj.length; i++) {
                obj[i][0] = this.menuItems.get(i).getId();
                obj[i][1] = this.menuItems.get(i).getName();
                obj[i][2] = this.menuItems.get(i).computePrice();
            }

            DefaultTableModel defaultTableModel = new DefaultTableModel(obj,columns);
            defaultTableModel.setNumRows(100);
//            for(MenuItem m:menuItems){
//                Object[] o = new Object[3];
//                o[0] = m.getId();
//                o[1] = m.getName();
//                o[2] = m.computePrice();
//                defaultTableModel.addRow(o);
//            }
            JTable table = new JTable(defaultTableModel);
            JTableHeader tableHeader = table.getTableHeader();
            tableHeader.setPreferredSize(new Dimension(50,20));
            JPanel jPanel = new JPanel();
            jPanel.setLayout(new GridLayout(2,1));
            jPanel.add(tableHeader);
            jPanel.add(table);
            cViewFrame.setContentPane(jPanel);
            cViewFrame.setVisible(true);
        });
    }
}
