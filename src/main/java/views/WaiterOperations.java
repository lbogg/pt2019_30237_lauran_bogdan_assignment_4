package views;

import logical.misc.RestaurantProcessing;
import logical.misc.RestaurantProcessingImpl;
import logical.pojos.BaseProduct;
import logical.pojos.CompositeProduct;
import logical.pojos.Order;
import logical.pojos.Table;
import logical.pojos.interfaces.MenuItem;
import logical.users.Admin;
import logical.users.interfaces.AdminGUI;
import logical.users.interfaces.WaiterGUI;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.text.BadLocationException;
import java.awt.*;
import java.util.*;
import java.util.List;

public class WaiterOperations extends JFrame {

    private JPanel jPanel;
    private JButton addBtn, computeBtn, viewAllBtn;
    private JTextField tableTxt;
    private JTextArea menuitems;
    private RestaurantProcessing restaurantProcessing;
    private WaiterGUI waiter;
    private List<MenuItem> menuItems;
    private JComboBox jComboBox;
    List<MenuItem> items = new ArrayList<>();

    public WaiterOperations(RestaurantProcessing restaurantProcessing, WaiterGUI waiter){
        this.setSize(new Dimension(400,400));
        this.setTitle("Waiter operations");
        this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        this.restaurantProcessing = restaurantProcessing;
        this.waiter = waiter;

        initElements();
    }

    private void initElements(){
        this.addBtn = new JButton("ADD");
        this.computeBtn = new JButton("COMPUTE BILL");
        this.viewAllBtn = new JButton("SHOW ALL");
        this.menuitems = new JTextArea("Order");
        this.tableTxt = new JTextField("Table number");
        this.jPanel = new JPanel();
        this.menuItems = restaurantProcessing.getRestaurant().getMenu();
        this.jComboBox = new JComboBox();

        createListeners();
    }

    private void createListeners(){
        jPanel.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();

        c.ipady = 40;
        c.gridwidth = 3;
        c.gridx = 0;
        c.gridy = 1;
        //jPanel.add(menuitems,c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0.5;
        c.gridx = 1;
        c.gridy = 0;
        jPanel.add(tableTxt,c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0.5;
        c.gridx = 1;
        c.gridy = 2;
        jPanel.add(jComboBox,c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0.0;
        c.gridx = 1;
        c.gridy = 3;
        jPanel.add(addBtn,c);
        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0.0;
        c.gridx = 2;
        c.gridy = 4;
        jPanel.add(viewAllBtn,c);
        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0.0;
        c.gridx = 2;
        c.gridy = 5;
        jPanel.add(computeBtn,c);

        this.add(jPanel);

        for(int i=3; i<menuItems.size(); i+=3){
            jComboBox.addItem(menuItems.get(i).getName());
        }

        addBtn.addActionListener(e -> {
            Table table = new Table();
            table.setNumber(Integer.parseInt(tableTxt.getText()));
            CompositeProduct product = new CompositeProduct(jComboBox.getSelectedItem().toString(),menuItems);
            items.add(product);
            Order order = new Order(table,items);
            waiter.saveOrder(order);
        });

        computeBtn.addActionListener(e -> {
            for(Order o:restaurantProcessing.getRestaurant().getOrders()){
                if(o.getTable().getNumber() == Integer.parseInt(tableTxt.getText())){
                    waiter.computeBill(o);
                }
            }
        });

        viewAllBtn.addActionListener(e -> {
            JFrame cViewFrame = new JFrame();
            cViewFrame.setSize(new Dimension(800,400));
            cViewFrame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

            String[] columns = {"Id","Table","Products"};
            Object[][] obj = new Object[restaurantProcessing.getRestaurant().getOrders().size()][3];

            for (int i = 0; i < obj.length; i++) {
                obj[i][0] = this.restaurantProcessing.getRestaurant().getOrders().get(i).getId();
                obj[i][1] = this.restaurantProcessing.getRestaurant().getOrders().get(i).getTable().getNumber();
                obj[i][2] = this.restaurantProcessing.getRestaurant().getOrders().get(i).getProducts().get(i).getName();
            }

            DefaultTableModel defaultTableModel = new DefaultTableModel(obj,columns);
            defaultTableModel.setNumRows(100);
//            for(MenuItem m:menuItems){
//                Object[] o = new Object[3];
//                o[0] = m.getId();
//                o[1] = m.getName();
//                o[2] = m.computePrice();
//                defaultTableModel.addRow(o);
//            }
            JTable table = new JTable(defaultTableModel);
            JTableHeader tableHeader = table.getTableHeader();
            tableHeader.setPreferredSize(new Dimension(50,20));
            JPanel jPanel = new JPanel();
            jPanel.setLayout(new GridLayout(2,1));
            jPanel.add(tableHeader);
            jPanel.add(table);
            cViewFrame.setContentPane(jPanel);
            cViewFrame.setVisible(true);
        });
    }
}
