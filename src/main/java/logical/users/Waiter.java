package logical.users;

import logical.misc.RestaurantProcessing;
import logical.pojos.Order;
import logical.users.interfaces.WaiterGUI;

import java.util.logging.Logger;

public class Waiter implements WaiterGUI {
    private RestaurantProcessing restaurantProcessing;

    public Waiter(RestaurantProcessing restaurantProcessing) {
        this.restaurantProcessing = restaurantProcessing;
    }

    @Override
    public void saveOrder(Order order) {
        Logger.getLogger(this.getClass().getSimpleName()).info("Saved order: " + order.getId());

        this.restaurantProcessing.saveOrder(order);
    }

    @Override
    public void computeBill(Order order) {
        this.restaurantProcessing.computeBill(order);
    }
}
