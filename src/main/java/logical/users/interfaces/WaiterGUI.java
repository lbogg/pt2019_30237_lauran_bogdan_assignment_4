package logical.users.interfaces;

import logical.pojos.Order;
import logical.pojos.interfaces.MenuItem;

public interface WaiterGUI {

    void saveOrder(Order order);
    void computeBill(Order order);
}
