package logical.users.interfaces;

import logical.pojos.interfaces.MenuItem;

public interface AdminGUI {
    void saveMenuItem(MenuItem menuItem);
    void deleteMenuItem(MenuItem menuItem);
}
