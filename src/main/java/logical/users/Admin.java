package logical.users;

import logical.misc.RestaurantProcessing;
import logical.pojos.interfaces.MenuItem;
import logical.users.interfaces.AdminGUI;

import java.util.logging.Logger;

public class Admin implements AdminGUI {
    private RestaurantProcessing restaurantProcessing;

    public Admin(RestaurantProcessing restaurantProcessing) {
        this.restaurantProcessing = restaurantProcessing;
    }

    @Override
    public void saveMenuItem(MenuItem menuItem) {
        Logger.getLogger(this.getClass().getSimpleName()).info("Saved menu item: " + menuItem.getName());

        this.restaurantProcessing.saveMenuItem(menuItem);
    }

    @Override
    public void deleteMenuItem(MenuItem menuItem) {
        Logger.getLogger(this.getClass().getSimpleName()).info("Deleted menu item: " + menuItem.getName());

        this.restaurantProcessing.deleteMenuItem(menuItem);
    }
}
