package logical.users;

import logical.pojos.CompositeProduct;
import logical.pojos.Order;
import logical.users.interfaces.ChefGUI;

import java.util.Observable;
import java.util.logging.Logger;

public class Chef implements ChefGUI {
    @Override
    public void update(Observable o, Object rawOrder) {
        Order order = (Order) rawOrder;

        order.getProducts().parallelStream()
                .filter(product -> product instanceof CompositeProduct)
                .forEach(product -> Logger.getLogger(this.getClass().getSimpleName()).info("Preparing product " + product.getName()));
    }


}
