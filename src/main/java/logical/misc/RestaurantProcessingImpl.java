package logical.misc;

import logical.data.FileWriter;
import logical.data.RestaurantSerializator;
import logical.data.BillSerialization;
import logical.pojos.Order;
import logical.pojos.Restaurant;
import logical.pojos.interfaces.MenuItem;


import java.util.logging.Logger;

public class RestaurantProcessingImpl implements RestaurantProcessing {
    private final static String FILE_DATA_NAME = "restaurant-data.txt";

    private Restaurant restaurant;
    private FileWriter fileWriter;
    private BillSerialization billSerializator;
    private RestaurantSerializator restaurantSerializator;

    public RestaurantProcessingImpl() {
        this.fileWriter = new FileWriter();
        this.billSerializator = new BillSerialization();
        this.restaurantSerializator = new RestaurantSerializator();

        this.loadData();
    }

    @Override
    public void saveMenuItem(MenuItem menuItem) {
        this.restaurant.getMenu().add(menuItem);
    }

    @Override
    public void deleteMenuItem(MenuItem menuItem) {
        this.restaurant.getMenu().removeIf(item -> item.getId() == menuItem.getId());
    }

    @Override
    public void computeBill(Order order) {
        String data = billSerializator.map(order);
        fileWriter.write("bill_" + order.getId() + ".txt", data);
    }

    @Override
    public double computePrice(Order order) {
        return order.getProducts().parallelStream().mapToDouble(MenuItem::computePrice).sum();
    }

    @Override
    public void saveOrder(Order order) {
        this.restaurant.addOrder(order);
    }

    @Override
    public void loadData() {
        Logger.getLogger(this.getClass().getSimpleName()).info("Loading restaurant data from file");


        String data = fileWriter.read(FILE_DATA_NAME);

        if (data == null) {
            this.restaurant = new Restaurant();
            return;
        }

        this.restaurant = this.restaurantSerializator.map(data);
    }

    @Override
    public void saveData() {
        Logger.getLogger(this.getClass().getSimpleName()).info("Saving restaurant data to file");
        this.fileWriter.write(FILE_DATA_NAME, this.restaurantSerializator.map(this.restaurant));
    }

    public Restaurant getRestaurant() {
        return this.restaurant;
    }
}
