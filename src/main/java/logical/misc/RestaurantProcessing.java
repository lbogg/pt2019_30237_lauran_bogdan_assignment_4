package logical.misc;


import logical.pojos.Order;
import logical.pojos.Restaurant;
import logical.pojos.interfaces.MenuItem;

public interface RestaurantProcessing {
    void saveMenuItem(MenuItem menuItem);
    void deleteMenuItem(MenuItem menuItem);

    void computeBill(Order order);
    double computePrice(Order order);
    void saveOrder(Order order);

    void loadData();
    void saveData();

    Restaurant getRestaurant();
}
