package logical.pojos;

import logical.pojos.interfaces.AbstractMenuItem;
import logical.pojos.interfaces.MenuItem;

import java.util.List;


public class CompositeProduct extends AbstractMenuItem {
    private List<MenuItem> composedOf;

    public CompositeProduct(String name, List<MenuItem> composedOf) {
        super(name);
        this.composedOf = composedOf;
    }

    @Override
    public double computePrice() {
        return composedOf.parallelStream().mapToDouble(MenuItem::computePrice).sum();
    }

    public List<MenuItem> getComposedOf() {
        return this.composedOf;
    }

    public MenuItem setPrice(double price) {
        throw new RuntimeException("Composite products have computed prices");
    }
}
