package logical.pojos;

import logical.pojos.interfaces.AbstractMenuItem;
import logical.pojos.interfaces.MenuItem;

public class BaseProduct extends AbstractMenuItem {
    private double price;

    public BaseProduct(String name, double price) {
        super(name);
        this.price = price;
    }

    public MenuItem setPrice(double price) {
        this.price = price;
        return this;
    }

    public double computePrice() {
        return price;
    }
}
