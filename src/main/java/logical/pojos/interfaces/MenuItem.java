package logical.pojos.interfaces;

public interface MenuItem {
    int getId();
    String getName();
    double computePrice();

    MenuItem setPrice(double price);
    MenuItem setName(String name);
}
