package logical.pojos.interfaces;

import java.util.Objects;

public abstract class AbstractMenuItem implements MenuItem {
    private static int lastId = 0;

    public AbstractMenuItem(String name) {
        this.id = ++AbstractMenuItem.lastId;
        this.name = name;
    }

    protected int id;
    protected String name;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public MenuItem setName(String name) {
        this.name = name;

        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AbstractMenuItem that = (AbstractMenuItem) o;
        return id == that.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
