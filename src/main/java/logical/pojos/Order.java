package logical.pojos;

import logical.pojos.interfaces.MenuItem;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Order {
    private static int lastId = 0;

    public Order(Table table, List<MenuItem> products) {
        this.table = table;
        this.products = products;
        this.id = ++Order.lastId;
        this.date = LocalDateTime.now();
    }

    public Order(Table table) {
        this(table, new ArrayList<>());
    }

    private int id;
    private LocalDateTime date;
    private Table table;
    private List<MenuItem> products;

    public int getId() {
        return id;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public Table getTable() {
        return table;
    }

    public List<MenuItem> getProducts() {
        return products;
    }

    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return id == order.id;
    }

    public int hashCode() {
        return Objects.hash(id);
    }
}
