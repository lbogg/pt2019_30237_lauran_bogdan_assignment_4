package logical.pojos;

import logical.pojos.interfaces.MenuItem;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

public class Restaurant extends Observable {
    private List<MenuItem> menu;
    private List<Order> orders;

    public Restaurant() {
        this.menu = menu = new ArrayList<>();
        this.orders = orders = new ArrayList<>();
    }

    public void addOrder(Order order) {
        this.getOrders().add(order);

        if (order.getProducts().parallelStream().anyMatch(product -> product instanceof CompositeProduct)) {
            this.setChanged();
            this.notifyObservers(order);
        }

    }

    public List<MenuItem> getMenu() {
        return menu;
    }

    public List<Order> getOrders() {
        return orders;
    }
}
