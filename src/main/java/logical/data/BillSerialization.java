package logical.data;

import logical.pojos.Order;
import com.google.gson.Gson;

public class BillSerialization {
    public String map(Order order) {
        return new Gson().toJson(order);
    }
}
