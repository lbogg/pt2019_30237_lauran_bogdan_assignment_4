package logical.data;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class FileWriter {
    public void write(String filename, String data) {
        File file = new File(filename);

        if (file.exists()) {
            file.delete();
        }

        try {
            java.io.FileWriter fw = new java.io.FileWriter(filename);
            fw.write(data);
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    public String read(String filename) {
        File file = new File(filename);

        if (!file.exists()) {
            return null;
        }


        try {
            return String.join("\n", Files.readAllLines(Paths.get(filename)));
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }

        return null;
    }
}
