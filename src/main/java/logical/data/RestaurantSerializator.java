package logical.data;

import com.owlike.genson.Genson;
import logical.pojos.Restaurant;

public class RestaurantSerializator {
    public Restaurant map(String data) {
        return new Genson.Builder().setWithClassMetadata(true).create().deserialize(data, Restaurant.class);
    }

    public String map(Restaurant restaurant) {
        return new Genson.Builder().setWithClassMetadata(true).create().serialize(restaurant);
    }

}
