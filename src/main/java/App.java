import logical.misc.RestaurantProcessing;
import logical.misc.RestaurantProcessingImpl;
import logical.pojos.BaseProduct;
import logical.pojos.CompositeProduct;
import logical.pojos.Order;
import logical.pojos.Table;
import logical.pojos.interfaces.MenuItem;
import logical.users.Admin;
import logical.users.Chef;
import logical.users.Waiter;
import logical.users.interfaces.AdminGUI;
import logical.users.interfaces.ChefGUI;
import logical.users.interfaces.WaiterGUI;
import views.MainView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class App {
    private static List<MenuItem> mockProducts() {
        List<MenuItem> out = new ArrayList<>();

        BaseProduct cheese = new BaseProduct("Cheese", 5);
        BaseProduct pepperoni = new BaseProduct("Pepperoni", 3);
        BaseProduct oregano = new BaseProduct("Oregano", 0.5);

        CompositeProduct pizza = new CompositeProduct("Pizza", Arrays.asList(cheese, pepperoni, oregano));

        Collections.addAll(out, cheese, pepperoni, oregano, pizza);
        return out;
    }

    public static void main( String[] args ) {
        RestaurantProcessing restaurantProcessing = new RestaurantProcessingImpl();
        WaiterGUI waiter = new Waiter(restaurantProcessing);
        AdminGUI admin = new Admin(restaurantProcessing);
        ChefGUI chef = new Chef();
        Table someTable = new Table();

        restaurantProcessing.getRestaurant().addObserver(chef);

        // called only if the restaurant data file doesn't exist
        if (restaurantProcessing.getRestaurant().getMenu().isEmpty()) {
            // create and add menu items
            List<MenuItem> menuItems = mockProducts();
            menuItems.forEach(admin::saveMenuItem);
        }

        // simulate order
        Order order = new Order(someTable, Arrays.asList(mockProducts().get(3 /* pizza */ )));
        waiter.saveOrder(order);
        waiter.computeBill(order);

        restaurantProcessing.saveData();

        MainView mainView = new MainView(restaurantProcessing, admin, waiter, chef);
        mainView.setVisible(true);
    }
}
